'use strict';

const authService = require("./authservice/AuthService");
const adminService = require("./adminservice/AdminService");




exports.handler = (event, context, callback) => {
    const done = function (err, res ) {
    var rect = {
        Error:"",
        TypeError:"",
        Object:null
    };
    if(err) {
        rect.Error = err.Error;
        rect.TypeError = err.TypeError;
    } else
        rect.Object = res;

    callback(null, {
        statusCode: err ? '400' : '200',
        body: JSON.stringify(rect),
        headers: {'Content-Type': 'application/json'}
    });
};

function action(err,res){ // se c'è stato un'errore è dato da verify login e quindi è un problema di accesso
    if (err)
        done(err,null);
        //quà dovrebbe fermarsi
    switch (event.httpMethod) {
        case 'GET':
            adminService.getAdmins(done);
            break;
        case 'POST':
            var body = JSON.parse(event.body);
            var check = adminService.validateAdmin(body);
            if(check.valid){
                adminService.addAdmin(body, done);
            } else
                done({Error:check.errors,TypeError:"data not valid"},null);
            break;
        case 'PUT': // if success return api trame with obj null
            var body = JSON.parse(event.body);
            var check = adminService.validateUpdate(body);
            if(check.valid){
                adminService.updateAdmin(body, done);
            } else
                done({Error:check.errors,TypeError:"data not valid"},null);
            break;
        case 'DELETE': // if success return api trame with obj null
            //controllare validità
            var body = JSON.parse(event.body);
            var check = adminService.validateUpdate(body);
                if(check.valid){
                adminService.deleteAdmin(body.username,done);
            } else
                done({Error:check.errors,TypeError:"data not valid"},null);
            
            break;
        default:
            done({Error:"unexist resource",TypeError:"unexist resource"},null);
            break;
    }
}

    if(event.headers.hasOwnProperty('token'))
        authService.verifyLogin(event.headers.token,function(err2,res2){
            if(!err2 && res2.superadmin)
                action(null,res2);
            else
                done({Error:"missed token", TypeError:"insufficient permission"},null)
        }); //controllare se superadmin
    else
        done({Error:"missed token", TypeError:"Invalid header"},null);
};
