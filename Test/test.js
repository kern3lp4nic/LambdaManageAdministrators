'use strict';
const fs = require("../index.js");


var chai = require("chai");
var assert = chai.assert;
var expect = chai.expect;

var tkn="ciaoi";
const event2={
    httpMethod:"GET",
    headers:{token:"ciaoi"}
}
const eventPOST={
    httpMethod:"POST",
    headers:{token:"ciaoi"},
    body:"{\"username\":\"powerhiew\",\"email\":\"zanonedoardo@gmail.com\",\"password\":\"owerelkjrew\",\"superadmin\":true}"
};
const eventDELETE={
    httpMethod:"DELETE",
    headers:{token:"ciaoi"},
body:"{\"username\":\"powerhiew\",\"email\":\"zanonedoardo@gmail.com\",\"password\":\"owerelkjrew\"}"
};
const eventPUT={
    httpMethod:"PUT",
    headers:{token:"ciaoi"},
    body:"{\"username\":\"powerhiew\",\"email\":\"zanonedoardo@gmail.com\",\"password\":\"owerelkjrew\"}"
}

var expect1={
  "Error": "",
  "TypeError": "",
  "Object": null
};



describe("ManageAdministrators", function () {
    context("request call GET ", function () {
        it("ok request", function (done) {
            fs.handler(event2,{}, function (err,data) {
                expect(err).to.equal(null);
                var res=JSON.parse(data.body);
                expect(data.statusCode).to.equal('200');
                expect(res.Object).to.not.equal(null);
                expect(res.Error).to.equal("");
                expect(res.TypeError).to.equal("");
                done();
            });
        });
    });

    context("request call POST ", function () {
        it("ok request", function (done) {
            fs.handler(eventPOST,{}, function (err,data) {
                expect(err).to.equal(null);
                expect(data).to.deep.equal({
                    statusCode: '200',
                    body: JSON.stringify(expect1),
                    headers: {'Content-Type': 'application/json'}
                });
                done();
            });
        });
    });

    context("request call PUT ", function () {
        it("ok request", function (done) {
            fs.handler(eventPUT,{}, function (err,data) {
                expect(err).to.equal(null);
                expect(data).to.deep.equal({
                    statusCode: '200',
                    body: JSON.stringify(expect1),
                    headers: {'Content-Type': 'application/json'}
                });
                done();
            });
        });
    });
    context("request call DELETE ", function () {
        it("ok request", function (done) {
            fs.handler(eventDELETE,{}, function (err,data) {
                expect(err).to.equal(null);
                expect(data).to.deep.equal({
                    statusCode: '200',
                    body: JSON.stringify(expect1),
                    headers: {'Content-Type': 'application/json'}
                });
                done();
            });
        });
    });
});